package ru.shem.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.shem.data.structures.JournalRow;

@Repository
public interface JournalRepository extends CrudRepository<JournalRow, Long> {
}
