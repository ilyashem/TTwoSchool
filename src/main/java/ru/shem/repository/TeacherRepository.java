package ru.shem.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.shem.data.souls.Teacher;

@Repository
public interface TeacherRepository extends CrudRepository<Teacher, Integer> {
}
