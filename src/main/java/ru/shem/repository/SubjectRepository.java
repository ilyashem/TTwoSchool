package ru.shem.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.shem.data.structures.Subject;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, Integer> {
}
