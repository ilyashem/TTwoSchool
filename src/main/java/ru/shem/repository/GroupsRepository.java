package ru.shem.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.shem.data.structures.Group;

@Repository
public interface GroupsRepository extends CrudRepository<Group, Integer> {
}
