package ru.shem.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.shem.data.souls.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Integer> {

}
