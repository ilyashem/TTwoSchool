package ru.shem.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shem.data.souls.Student;
import ru.shem.data.souls.Teacher;
import ru.shem.data.structures.Group;
import ru.shem.data.structures.JournalRow;
import ru.shem.data.structures.Subject;
import ru.shem.repository.GroupsRepository;
import ru.shem.repository.JournalRepository;

import java.util.Date;
import java.util.LinkedList;
import java.util.Optional;

@Service
public class GroupService {

    @Autowired
    private GroupsRepository groupsRepository;

    @Autowired
    private JournalRepository journalRepository;

    public Iterable<Group> getGroups() {
        return groupsRepository.findAll();
    }

    public Group createGroup(Iterable<Student> studentsList, Teacher classroomTeacher) {
        Group newGroup = new Group();

        classroomTeacher.setGroup(newGroup);
        for(Student student : studentsList) {
            student.setGroup(newGroup);
        }

        groupsRepository.save(newGroup);

        return newGroup;
    }

    public JournalRow addRowToGroupJournal(Student student, Subject subject, short score) {
        return journalRepository.save(new JournalRow(student, subject, score));
    }

    public JournalRow modifyScoreInJournal(Long rowId, short score) {
        Optional<JournalRow> rowOptional = journalRepository.findById(rowId);
        JournalRow chosenRow = null;

        if(rowOptional.isPresent()) {
            chosenRow = rowOptional.get();
            chosenRow.setScore(score);
            chosenRow.setUpdated(new Date());
        }

        assert chosenRow != null;
        chosenRow = journalRepository.save(chosenRow);

        return chosenRow;
    }

    public Iterable<JournalRow> getGroupJournal(int chosenGroupId) {
        LinkedList<JournalRow> groupJournal = new LinkedList<>();

        for(JournalRow row : journalRepository.findAll()) {
            if(row.getGroup().getGroupId() == chosenGroupId) {
                groupJournal.add(row);
            }
        }

        return groupJournal;
    }
}
