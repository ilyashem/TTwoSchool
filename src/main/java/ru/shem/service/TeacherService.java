package ru.shem.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shem.data.souls.Teacher;
import ru.shem.repository.TeacherRepository;

import java.util.Iterator;
import java.util.Optional;

@Service
public class TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;

    public Teacher addTeacher(Teacher teacher) {
        teacherRepository.save(teacher);
        return teacher;
    }

    public Iterable<Teacher> addAllTeacher(Iterable<Teacher> listForAdding) {
        return teacherRepository.saveAll(listForAdding);
    }

    public Teacher deleteTeacher(int id) {
        Teacher deletedTeacher = null;

        if (teacherRepository.findById(id).isPresent()) {
            deletedTeacher = teacherRepository.findById(id).get();
            teacherRepository.delete(deletedTeacher);
        }

        return deletedTeacher;
    }

    public Iterable<Teacher> deleteAllTeachers() {
        teacherRepository.deleteAll();
        return teacherRepository.findAll();
    }

    public Iterable<Teacher> getTeachers(){
        return teacherRepository.findAll();
    }

    public Teacher getTeacherById(int teacherId) {
        Optional<Teacher> requestResult = teacherRepository.findById(teacherId);
        return requestResult.orElse(null);
    }

    public Long countAllTeachers() {
        return teacherRepository.count();
    }

    public Teacher getTeacherWithoutGroup() {
        Teacher teacherWithoutGroup = null;

        Iterator teacherIterator = getTeachers().iterator();
        while(teacherIterator.hasNext()) {
            Teacher teacher = (Teacher) teacherIterator.next();
            if(teacher.getGroup() == null) {
                teacherWithoutGroup = teacher;
                break;
            }
        }

        return teacherWithoutGroup;
    }
}
