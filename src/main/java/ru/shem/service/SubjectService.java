package ru.shem.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shem.data.structures.Subject;
import ru.shem.repository.SubjectRepository;

import java.util.Optional;

@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    public Subject addSubject(Subject newSubject) {
        return subjectRepository.save(newSubject);
    }

    public Iterable<Subject> getSubjectsList() {
        return subjectRepository.findAll();
    }

    public Subject getSubject(String subjectName) {
        for(Subject currentSubject : subjectRepository.findAll()) {
            if(currentSubject.getName().equals(subjectName)) {
                return currentSubject;
            }
        }

        return null;
    }

    public Subject getSubject(int subjectId) {
        Optional<Subject> subjectOptional = subjectRepository.findById(subjectId);
        return subjectOptional.orElse(null);
    }
}
