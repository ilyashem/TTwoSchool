package ru.shem.service;

import ru.shem.data.souls.Student;
import ru.shem.data.structures.Group;
import ru.shem.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public Student addStudent(Student student) {
        studentRepository.save(student);
        return student;
    }

    public Iterable<Student> addAllStudents(Iterable<Student> listForAdding) {
        return studentRepository.saveAll(listForAdding);
    }

    public Student deleteStudent(int id) {
        Student deletedStudent = null;

        if (studentRepository.findById(id).isPresent()) {
            deletedStudent = studentRepository.findById(id).get();
            studentRepository.delete(deletedStudent);
        }

        return deletedStudent;
    }

    public Iterable<Student> deleteAllStudents() {
        studentRepository.deleteAll();
        return studentRepository.findAll();
    }

    public Iterable<Student> getStudents(){
        return studentRepository.findAll();
    }

    public Student getStudent(String firstName, String lastName) {
        for (Student currentStudent : getStudents()) {
            if (currentStudent.getFullName().equals(firstName + ' ' + lastName)) {
                return currentStudent;
            }
        }

        return null;
    }

    public Student getStudent(int id) {
        Optional<Student> studentOptional = studentRepository.findById(id);
        return studentOptional.orElse(null);
    }

    public Long countAllStudents() {
        return studentRepository.count();
    }

    public LinkedList<Student> getStudentsWithoutGroup(Integer limit) {
        LinkedList<Student> studentsWithoutGroup = new LinkedList<>();

        for(Student student : studentRepository.findAll()) {
            if(student.getGroup() == null) {
                studentsWithoutGroup.add(student);
            }
            if((limit != null) && (studentsWithoutGroup.size() == limit)) {
                break;
            }
        }

        return studentsWithoutGroup;
    }

    public Iterable<Student> getGroupList(Group outputGroup) {
        LinkedList<Student> studentsOfChosenGroup = new LinkedList<>();
        Iterator<Student> studentsIterator =  studentRepository.findAll().iterator();

        while(studentsIterator.hasNext()) {
            Student currentStudent = studentsIterator.next();

            if(currentStudent.getGroup().equals(outputGroup)) {
                studentsOfChosenGroup.add(currentStudent);
            }
        }
        return studentsOfChosenGroup;
    }
}
