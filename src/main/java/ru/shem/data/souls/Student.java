package ru.shem.data.souls;

import ru.shem.data.structures.Group;

import javax.persistence.*;

@Entity
@Table(name="students", schema = "public")
public class Student {

    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    int id;

    String firstName;

    String lastName;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn(name = "group_id")
    Group group;

    public Student() {

    }

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Transient
    public String getFullName() {
        return firstName + ' ' + lastName;
    }
}
