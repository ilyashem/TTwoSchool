package ru.shem.data.structures;

import ru.shem.data.souls.Student;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="journal", schema = "public")
public class JournalRow {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    Long rowId;

    @ManyToOne
    Group group;

    @ManyToOne
    Student student;

    @ManyToOne
    Subject subject;

    short score;

    Date date;

    Date updated;

    public JournalRow() { }

    public JournalRow(Student student, Subject subject, short score) {
        this.student = student;
        this.group = student.getGroup();
        this.subject = subject;
        this.score = score;
        this.date = new Date();
    }

    public JournalRow(Student student, Subject subject, short score, Date date) {
        this.student = student;
        this.group = student.getGroup();
        this.subject = subject;
        this.score = score;
        this.date = date;
    }

    public Long getRowId() {
        return rowId;
    }

    public void setRowId(Long rowId) {
        this.rowId = rowId;
    }

    public Group getGroup() {
        return group;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
        this.group = student.getGroup();
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public short getScore() {
        return score;
    }

    public void setScore(short score) {
        this.score = score;
    }
}
