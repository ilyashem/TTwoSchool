package ru.shem.data.structures;

import javax.persistence.*;

@Entity
@Table(name="groups", schema = "public")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    int groupId;

    public Group() { }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}