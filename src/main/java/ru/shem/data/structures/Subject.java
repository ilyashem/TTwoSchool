package ru.shem.data.structures;

import ru.shem.data.souls.Teacher;

import javax.persistence.*;

@Entity
@Table(name="subjects", schema = "public")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int subjectId;

    String name;

    @ManyToOne
    Teacher teacher;

    public Subject() {

    }

    public Subject(String name, Teacher teacher) {
        this.name = name;
        this.teacher = teacher;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
