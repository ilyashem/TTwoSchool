package ru.shem.controller;

import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.shem.data.souls.Student;
import ru.shem.data.souls.Teacher;
import ru.shem.data.structures.Group;
import ru.shem.data.structures.Subject;
import ru.shem.service.GroupService;
import ru.shem.service.StudentService;
import ru.shem.service.SubjectService;
import ru.shem.service.TeacherService;
import ru.shem.util.NameGenerator;

import java.util.LinkedList;

@Controller
public class SchoolController {

    @Autowired
    private GroupService groupService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private SubjectService subjectService;

    private final int DEFAULT_GROUP_CAPACITY = 20;
    private final int DEFAULT_STUDENTS_COUNT = 100;
    private final int DEFAULT_TEACHERS_COUNT = 8;

    @RequestMapping(value = "getGroupsList", method = RequestMethod.GET)
    @ResponseBody Iterable<Group> getGroupsList() {
        return groupService.getGroups();
    }

    @RequestMapping(value = "getGroupListByStudent", method = RequestMethod.GET)
    ResponseEntity<?> getGroupListByStudent(@RequestParam String firstName,
                                            @RequestParam String lastName) {
        Student searchingOfStudent = studentService.getStudent(firstName, lastName);

        if(searchingOfStudent != null) {
            Group outputGroup = searchingOfStudent.getGroup();

            if (outputGroup != null) {
                return ResponseEntity.ok(studentService.getGroupList(outputGroup));
            } else {
                return ResponseEntity.badRequest().body(generateJsonResponse("У студента " + searchingOfStudent.getFullName() + " нет группы.."));
            }
        }

        return ResponseEntity.badRequest().body(generateJsonResponse("Такого студента не существует"));
    }

    @RequestMapping(value = "createGroups", method = RequestMethod.PUT)
    ResponseEntity<?> createGroups(@RequestParam int groupsAmount) {
        LinkedList<Group> newGroupList = new LinkedList<>();
        LinkedList<Student> studentsWithoutGroup;

        for (int i = 0; i < groupsAmount; i++) {
            studentsWithoutGroup = (LinkedList<Student>) studentService.getStudentsWithoutGroup(DEFAULT_GROUP_CAPACITY);

            if (studentsWithoutGroup.size() == 0) {
                return ResponseEntity.badRequest().body(generateJsonResponse("В школе нет учеников без группы"));
            } else if (studentsWithoutGroup.size() < DEFAULT_GROUP_CAPACITY) {
                return ResponseEntity.badRequest().body(generateJsonResponse("В школе недостаточно учеников без группы, наберите минимум "
                        + DEFAULT_GROUP_CAPACITY
                        + " учеников и тогда станет возможным создание группы\nУчеников без группы в данный момент: "
                        + studentsWithoutGroup.size()));
            } else {
                newGroupList.add(groupService.createGroup(studentsWithoutGroup,
                        teacherService.getTeacherWithoutGroup()));
            }
        }

        return ResponseEntity.ok(newGroupList);
    }

    @RequestMapping(value = "addRowIntoGroupJournal", method = RequestMethod.PUT)
    ResponseEntity<?> addRowIntoGroupJournal(@RequestParam int studentId,
                                             @RequestParam int subjectId,
                                             @RequestParam short score) {
        Student chosenStudent = studentService.getStudent(studentId);

        if(chosenStudent == null) {
            return ResponseEntity.badRequest().body(generateJsonResponse("Студента с номером " + studentId + " не существует"));
        }

        Subject chosenSubject = subjectService.getSubject(subjectId);

        if(chosenSubject == null) {
            return ResponseEntity.badRequest().body(generateJsonResponse("Предмета с номером " + subjectId + " не существует"));
        }

        return ResponseEntity.ok(groupService.addRowToGroupJournal(chosenStudent, chosenSubject, score));
    }

    @RequestMapping(value = "modifyRowInJournal", method = RequestMethod.PUT)
    ResponseEntity<?> modifyRowInJournal(@RequestParam Long rowId,
                                         @RequestParam short score) {
        return ResponseEntity.ok(groupService.modifyScoreInJournal(rowId, score));
    }

    @RequestMapping(value = "getGroupJournal", method = RequestMethod.GET)
    ResponseEntity<?> getGroupJournal(@RequestParam int groupId) {
        return ResponseEntity.ok(groupService.getGroupJournal(groupId));
    }

    @RequestMapping(value = "addStudent", method = RequestMethod.PUT)
    ResponseEntity<?> addStudent(@RequestParam String firstName,
                                 @RequestParam String lastName ) {
        try {
            Student student = new Student();
            student.setFirstName(firstName);
            student.setLastName(lastName);
            student = (Student) studentService.addStudent(student);
            return ResponseEntity.ok(student);
        }
        catch (Exception ex){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @RequestMapping(value = "generateStudents", method = RequestMethod.PUT)
    ResponseEntity<?> fillStudentsList() {
        LinkedList<Student> listForAdding = new LinkedList<>();

        int countForAdding = (int) (DEFAULT_STUDENTS_COUNT - studentService.countAllStudents());

        if(countForAdding > 0) {
            String[] names = NameGenerator.generateNamesArray(countForAdding);
            for (String name : names) {
                String[] nameParts = name.split(" ", 2);
                listForAdding.add(new Student(nameParts[0], nameParts[1]));
            }
            return ResponseEntity.ok(studentService.addAllStudents(listForAdding));
        } else {
            return ResponseEntity.badRequest().body(generateJsonResponse("Не было добавленно ни одного ученика, так как список учеников переполнен (Максимум учеников по умолчанию: " + DEFAULT_STUDENTS_COUNT + ")"));
        }
    }

    @RequestMapping(value = "deleteStudent", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteStudent(@RequestParam int id) {
        Student deletedStudent = studentService.deleteStudent(id);
        if(deletedStudent != null) {
            return ResponseEntity.ok(deletedStudent);
        } else {
            return ResponseEntity.badRequest().body(generateJsonResponse("Ученика с ID = " + id + " не существует"));
        }
    }

    @RequestMapping(value = "clearStudentsList", method = RequestMethod.DELETE)
    ResponseEntity<?> clearStudentsList() {
        return ResponseEntity.ok(studentService.deleteAllStudents());
    }

    @RequestMapping(value = "getStudents", method = RequestMethod.GET)
    ResponseEntity<?> getStudentsList() {
        return ResponseEntity.ok(studentService.getStudents());
    }

    @RequestMapping(value = "getStudentsWithoutGroup", method = RequestMethod.GET)
    ResponseEntity<?> getStudentsWithoutGroup() {
        LinkedList<Student> studentsWithoutGroup = studentService.getStudentsWithoutGroup(null);
        if(studentsWithoutGroup.isEmpty()) {
            return ResponseEntity.badRequest().body(generateJsonResponse("У всех студентов есть группа"));
        }
        return ResponseEntity.ok(studentsWithoutGroup);
    }

    @RequestMapping(value = "addSubject", method = RequestMethod.PUT)
    ResponseEntity<?> addSubject(@RequestParam String subjectName,
                                 @RequestParam int teacherId) {
        for (Subject subject : subjectService.getSubjectsList()) {
            if (subject.getName().equals(subjectName)) {
                return ResponseEntity.badRequest().body(generateJsonResponse("Предмет с названием \"" + subjectName + "\" уже существует"));
            }
        }

        Teacher chosenTeacher = teacherService.getTeacherById(teacherId);

        Subject newSubject = new Subject(subjectName, chosenTeacher);
        subjectService.addSubject(newSubject);

        return ResponseEntity.ok(newSubject);
    }

    @RequestMapping(value = "getSubjectsList", method = RequestMethod.GET)
    @ResponseBody Iterable<Subject> getSubjectsList() {
        return subjectService.getSubjectsList();
    }

    @RequestMapping(value = "addTeacher", method = RequestMethod.PUT)
    ResponseEntity<?> addTeacher(@RequestParam String firstName,
                                 @RequestParam String lastName ) {
        try {
            Teacher teacher = new Teacher();
            teacher.setFirstName(firstName);
            teacher.setLastName(lastName);
            teacher = (Teacher) teacherService.addTeacher(teacher);
            return ResponseEntity.ok(teacher);
        }
        catch (Exception ex){
            return ResponseEntity.badRequest().body(generateJsonResponse("Учитель не был добавлен"));
        }
    }

    @RequestMapping(value = "generateTeachers", method = RequestMethod.PUT)
    ResponseEntity<?> fillTeachersList() {
        LinkedList<Teacher> listForAdding = new LinkedList<>();

        int countForAdding = (int) (DEFAULT_TEACHERS_COUNT - teacherService.countAllTeachers());

        if(countForAdding > 0) {
            String[] names = NameGenerator.generateNamesArray(countForAdding);
            for (String name : names) {
                String[] nameParts = name.split(" ", 2);
                listForAdding.add(new Teacher(nameParts[0], nameParts[1]));
            }
            return ResponseEntity.ok(teacherService.addAllTeacher(listForAdding));
        } else {
            return ResponseEntity.badRequest().body(generateJsonResponse("Не было добавленно ни одного учителя, так как список учителей переполнен (Максимум учеников по умолчанию: " + DEFAULT_TEACHERS_COUNT + ")"));
        }
    }

    @RequestMapping(value = "deleteTeacher", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteTeacher(@RequestParam int id) {
        Teacher deletedTeacher = teacherService.deleteTeacher(id);
        if(deletedTeacher != null) {
            return ResponseEntity.ok(deletedTeacher);
        } else {
            return ResponseEntity.badRequest().body(generateJsonResponse("Учитель с ID = " + id + " не сущуствует"));
        }
    }

    @RequestMapping(value = "clearTeachersList", method = RequestMethod.DELETE)
    ResponseEntity<?> clearTeachersList() {
        return ResponseEntity.ok(teacherService.deleteAllTeachers());
    }

    @RequestMapping(value = "getTeachers", method = RequestMethod.GET)
    ResponseEntity<?> getTeachersList() {
        return ResponseEntity.ok(teacherService.getTeachers());
    }

    private JSONObject generateJsonResponse(String responseBody) {
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("response", responseBody);
        return jsonResponse;
    }
}
