package ru.shem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TTwoSchoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(TTwoSchoolApplication.class, args);
	}
}
